package com.company;

public class Information extends User {
    private String gender;
    private String nationality;
    private String age;
    private String contacts;


    public Information() {

    }

    public Information(String gender, String nationality) {
        setGender(gender);
        setNationality(nationality);
    }

    public Information(String gender, String nationality, String age) {
        this(gender, nationality);
        setAge(age);
    }

    public Information(String name, String surname, String gender, String nationality, String age, String contacts) {
        super(name, surname);
        setGender(gender);
        setNationality(nationality);
        setAge(age);
        setContacts(contacts);
    }


    public String getGender() {
        return gender;
    }

    public String getNationality() {
        return nationality;
    }

    public String getAge() {
        return age;
    }

    public String getContacts() {
        return contacts;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public void setContacts(String contacts) {
        this.contacts = contacts;
    }

    @Override
    public String toString() {
        return super.toString() + " " + gender + " " + nationality + " " + age + " " + contacts;
    }
}

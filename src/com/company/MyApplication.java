package com.company;

import java.io.*;
import java.nio.channels.FileChannel;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Scanner;

public class MyApplication {
    // users - a list of users
    private Scanner sc = new Scanner(System.in);
    BufferedReader read = new BufferedReader(new InputStreamReader(System.in));
    private User signedUser;
    private ArrayList<User> usersList = new ArrayList<User>();
    private ArrayList<Information> infoList = new ArrayList<Information>();


    private void addUser(User user) {
        usersList.add(user);
    }


    private void menu() {
        while (true) {
            if (signedUser == null) {
                System.out.println("You are not signed in.");
                System.out.println("1. Authentication");
                System.out.println("2. Exit");
                int choice = sc.nextInt();
                if (choice == 1) authentication();
                else break;
            } else {
                userProfile();
            }
        }
    }

    private void userProfile() {

        signedUser = null;
    }



    private void authentication() {
        System.out.println("1. Sign in");
        System.out.println("2. Sign up");
        System.out.println("3. Back to menu");
        System.out.println("4. Show users list");
        int choice = sc.nextInt();
        if (choice == 1) {
            signIn();
        }else if (choice == 2) {
            signUp();
        }else if (choice == 3) {
            menu();
        }else if (choice == 4) {
            showUsers();
        }
    }

    private void signIn() {
        System.out.println("Write your username: ");

        String username = null;
        try {
            username = read.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Write your password: ");

        String password = null;
        try {
            password = read.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (checkListUname(username) && checkListPass(password)) {
            System.out.println("Hello new user!");
            for (int i = 0; i < usersList.size(); i++) {
                User user = usersList.get(i);
                if (username.equals(user.getUsername())) {
                    signedUser = user;
                }
            }
            System.out.println("Please, enter your personal info, new user!");
            System.out.println("1. Enter");
            System.out.println("2. After");
            int ch = sc.nextInt();
            if (ch == 1) {
                yourInfo();
            } else if (ch == 2) {
                authentication();
            }
        } else {
            System.out.println("Sorry, you had a mistake or you haven't got any account.");
        }

    }

    private void yourInfo() {
        Information curr = new Information();
        System.out.println("Write your gender: ");
        String gender = null;
        try {
            gender = read.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        checkGender(gender);
        for(int i=0; i<usersList.size(); i++){
            if(usersList.get(i).getUsername().equals(infoList.get(i).getUsername())){
                infoList.get(i).setGender(gender);
            }
        }


        System.out.println("Write your nationality:");
        String nationality = null;
        try {
            nationality = read.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        for(int i=0; i<usersList.size(); i++){
            if(usersList.get(i).getUsername().equals(infoList.get(i).getUsername())){
                infoList.get(i).setNationality(nationality);
            }
        }

        System.out.println("Write your age:");
        String age = null;
        try {
            age = read.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        for(int i=0; i<usersList.size(); i++){
            if(usersList.get(i).getUsername().equals(infoList.get(i).getUsername())){
                infoList.get(i).setAge(age);
            }
        }

        System.out.println("Write your contacts");

        String contacts = null;
        try {
            contacts = read.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        for(int i=0; i<usersList.size(); i++){
            if(usersList.get(i).getUsername().equals(infoList.get(i).getUsername())){
                infoList.get(i).setContacts(contacts);
            }
        }
        System.out.println("Congratulations, your account was updated successfully!");

    }



    private void signUp() {
        User curr = new User();
        System.out.println("Write your name: ");

        String name = null;
        try {
            name = read.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        curr.setName(name);
        System.out.println("Write your surname:");

        String surname = null;
        try {
            surname = read.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        curr.setSurname(surname);
        if (signUpUname(curr) && signUpPass(curr)) {
            addUser(curr);
            signedUser = curr;
            System.out.println("Congratulations, your account was created successfully!" );
        } else return;
    }


    private boolean checkGender(String gender) {
        if(gender.equals("Men")  ||  gender.equals("Women")){
            return true;
        }else {
            System.out.println("Please, write your gender!");
            yourInfo();
        }
        return false;
    }

    private boolean signUpPass(User current) {
        System.out.println("Enter your password (it should contain uppercase and lowercase letters and digits and its length must be more than 9 symbols)");
        String password = null;
        try {
            password = read.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Password password1 = new Password();
        if (password1.setPassword(password) && !password.equals(current.getUsername())) {
            current.setPassword(password1);
            return true;
        } else {
            System.out.println("Your password had an incorrect format.");
            System.out.println("1. One more time ,try");
            System.out.println("2. Back");
            int choice = sc.nextInt();
            if (choice == 1) {
                signUpPass(current);
            } else {
                return false;
            }
        }
        return true;
    }

    private boolean signUpUname(User current) {
        System.out.println("Enter your username:");
        String username = null;
        try {
            username = read.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (checkListUname(username)) {
            System.out.println("Change your username, someone already sign in with this username.");
            System.out.println("1. One more time ,try");
            System.out.println("2. Back");
            int choice = sc.nextInt();
            if (choice == 1) {
                signUpUname(current);
            } else {
                return false;
            }
        } else {
            current.setUsername(username);
            return true;
        }
        return true;
    }


    private boolean checkListUname(String username) {
        if (!usersList.isEmpty()) {
            for (int i = 0; i < usersList.size(); i++) {
                User user = usersList.get(i);
                if (username.equals(user.getUsername())) {
                    return true;
                }
            }
        }
        return false;
    }


    private boolean checkListPass(String password) {
        if (!usersList.isEmpty()) {
            for (int i = 0; i < usersList.size(); i++) {
                User user = usersList.get(i);
                if (password.equals(user.getPassword().getPassword())) {
                    return true;
                }
            }
        }
        return false;
    }


    public void start() throws FileNotFoundException {
        File file = new File("C:\\Users\\Райымбек\\IdeaProjects\\practice1\\db.txt");
        Scanner fileScanner = new Scanner(file);
        FillListWithUsers();






        while (true) {
            System.out.println("Welcome to my application!");
            System.out.println("Select command:");
            System.out.println("1. Menu");
            System.out.println("2. Exit");
            int choice = sc.nextInt();
            if (choice == 1) {
                menu();
            } else if(choice == 2){
                try {
                    FileChannel.open(Paths.get(String.valueOf(file)), StandardOpenOption.WRITE).truncate(0).close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                saveUsersList();
                break;
            }

        }


    }
    private void saveInfoList() {
        for(int i=0; i<usersList.size(); i++){
            int id = usersList.get(i).getId();
            String name = usersList.get(i).getName();
            String surname = usersList.get(i).getSurname();
            String username = usersList.get(i).getUsername();
            String password = usersList.get(i).getPassword().getPassword();
            Information info = new Information();
            info.setName(name);
            info.setSurname(surname);
            info.setUsername(username);

            Password passwordObj = new Password();
            passwordObj.setPassword(password);
            info.setPassword(passwordObj);

            infoList.add(info);
        }
        System.out.println(infoList);
    }


    private void saveUsersList() {
        File file = new File("C:\\Users\\Райымбек\\IdeaProjects\\practice1\\db.txt");
        try {
            for (int i = 0; i < infoList.size(); i++) {
                int id = infoList.get(i).getId();
                String name = infoList.get(i).getName();
                String surname = infoList.get(i).getSurname();
                String username = infoList.get(i).getUsername();
                String password = infoList.get(i).getPassword().getPassword();
                String gender = infoList.get(i).getGender();
                String nationality = infoList.get(i).getNationality();
                String age = infoList.get(i).getAge();
                String contacts = infoList.get(i).getContacts();
                PrintWriter db = new PrintWriter(new BufferedWriter(new FileWriter(file, true)));
                db.println(id + " " + name + " " + surname + " " + username + " " + password + " " + gender + " " + nationality + " " + age + " " + contacts );
                db.flush();
                db.close();

            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void FillListWithUsers() throws FileNotFoundException {
        File file = new File("C:\\Users\\Райымбек\\IdeaProjects\\practice1\\db.txt");
        Scanner reader = new Scanner(file);
        while (reader.hasNext()) {
            int id = reader.nextInt();
            String name = reader.next();
            String surname = reader.next();
            String username = reader.next();
            String password = reader.next();

            User user = new User();

            Password passwordObj = new Password();
            user.setId(id);
            user.setName(name);
            user.setSurname(surname);
            user.setUsername(username);
            passwordObj.setPassword(password);

            user.setPassword(passwordObj);

            usersList.add(user);


        }


        reader.close();
    }
    public void showUsers(){
        if(!usersList.isEmpty() ) {
            for (int i = 0; i < usersList.size(); i++) {
                    System.out.println(usersList.get(i));

            }

        }
    }
}



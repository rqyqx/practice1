package com.company;

public class Password {
    private String password;

    public Password() {
    }

    public boolean setPassword(String password) {
        if (!isValid(password)) {
            return false;
        } else {
            this.password = password;
            return true;
        }
    }

    public String getPassword() {
        return password;
    }

    public static boolean isValid(String password) {
        int counter1 = 0;
        int counter2 = 0;
        int counter3 = 0;
        int counter4 = 0;
        int counter5 = 0;

        if (password.length() >= 9) {
            for (int i = 0; i < password.length(); i++) {
                if (password.charAt(i) >= 65 && password.charAt(i) <= 90) {
                    counter1++;
                }
                if (password.charAt(i) >= 97 && password.charAt(i) <= 122) {
                    counter2++;
                }
                if (password.charAt(i) >= 33 && password.charAt(i) <= 47) {
                    counter3++;
                }
                if (password.charAt(i) >= 48 && password.charAt(i) <= 57) {
                    counter4++;
                }

            }
            if (counter1>0 && counter2>0 && counter3>0 && counter4>0) {
                return true;
            } else {
                return false;
            }
        }
            return false;

    }
}
package com.company;

public class User {
    private int id;
    private static int id_gen = 0;
    public String name;
    public String surname;
    private String username;
    private Password password;



    public User() {
        this.id = id_gen++;
    }

    public User(String name, String surname) {
        setName(name);
        setSurname(surname);
    }

    public User(String name, String surname, String username) {
        this(name, surname);
        setUsername(username);
    }

    public User(String name, String surname, String username, Password password) {
        this(name, surname, username);
        setPassword(password);
    }

    public int getId() {

        return this.id;
    }
    public void setId(int id) {
        this.id = id;
    }


    public String getName() {

        return this.name;
    }

    public void setName(String name) {

        this.name = name;
    }

    public String getSurname() {

        return this.surname;
    }

    public void setSurname(String surname) {

        this.surname = surname;
    }

    public String getUsername() {

        return this.username;
    }

    public void setUsername(String username) {

        this.username = username;
    }

    public Password getPassword() {
        return password;
    }

    public void setPassword(Password password) {

        this.password = password;
    }




    @Override
    public String toString() {
        return this.id + " " + this.name + " " + this.surname + " " + this.username + " " + password.getPassword();
    }
}
